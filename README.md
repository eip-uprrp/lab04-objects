#Lab. 4: Utilizando objetos en C++

Hasta ahora hemos visto como utilizar variables para guardar y manipular datos de cierto tipo y como estructurar nuestros programas dividiendo las tareas en funciones. En la experiencia de laboratorio de hoy repasaremos los conceptos básicos del ente llamado *objeto* que contiene los datos y los procedimientos que se usan para manipularlos. También repasaremos los componentes del "diseño" que tiene un objeto y que se definen en su *clase*. Practicaremos la creación y manipulación de objetos de una clase llamada `Birds`. 


#Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes habrán reforzado su conocimiento de conceptos básicos sobre objetos y clases. Habrán practicado la creación y manipulación de objetos de una clase llamada `Bird`. 


#Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los conceptos básicos relacionados a objetos y clases en C++

2. haber estudiado la documentación de la clase `Bird` disponible en [este enlace.](http://ada.uprrp.edu/~ranazario/bird-html/class_bird.html)

3. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

4. haber tomado el [quiz Pre-Lab 4](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6653) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).


#Sesión de laboratorio:

##Clases y Objetos en C++

Al igual que cada variable tiene un *tipo* de dato asociada a ella, cada objeto tiene una *clase* asociada que describe las propiedades del objeto:
sus datos (*atributos*), y los procedimientos que se pueden hacer a los datos (*funciones miembro*).

Para definir y utilizar un objeto  no hay que saber todos los detalles de los procedimientos del objeto pero hay que saber que datos contiene, que procedimientos se pueden hacer y como interactuar con el objeto. Esta información está disponible en la documentación de la clase. Antes de crear objetos de cualquier clase debemos familiarizarnos con su documentación. La documentación nos indica, entre otras cosas, que ente se está tratando de representar con la clase, y cuales son los interfaces  o funciones disponibles para manipular los objetos de la clase.

Dale un vistazo a la documentación de la clase `Bird` que se encuentra [aquí.](http://ada.uprrp.edu/~ranazario/bird-html/class_bird.html)

###Clases

Una clase es un pedazo de código en donde se describe cómo serán los objetos. Se definen los atributos de los datos que contendrá el objeto y las funciones que hacen algún procedimiento a los datos del objeto. Para declarar una clase debemos especificar los tipos que tendrán las variables y las funciones de la clase. 

En la experiencia de laboratorio de funciones definimos objetos que eran instancias de la clase `string` y utilizamos las funciones miembro de esta clase `length` y `push_back`.

Si no se especifica lo contrario, las variables y funciones definidas en una clase serán "privadas". Esto quiere decir que esas variables solo se pueden acceder y cambiar por las funciones miembro de la clase (*constructores*, *"setters"* y *"getters"*, entre otras). 

Lo siguiente es el esqueleto de la declaración de una clase:

```
  class NombreClase
   {
    // Declaraciones

    private:
      // Declaraciones de variables miembro y 
      // prototipos de funciones miembro 
      // que sean privadas para esta clase

      tipo varPrivada;
      tipoDevolver nombreFunciónPrivada(tipo de los parámetros);

    public:
      // Declaraciones de variables miembro y 
      // prototipos de funciones miembro 
      // que sean públicas para todo el programa

      tipo varPública;
      tipoDevolver nombreFunciónPública(tipo de los parámetros);
   };
```


Puedes ver la declaración de la clase `Birds` en el archivo `bird.h` incluido en el proyecto `Lab04-Objetcs`.


###Objetos

Un objeto es un ente que contiene datos (al igual que una variable), llamados sus `atributos`, y también contiene procedimientos, llamados `funciones miembro`, que se usan para manipularlos. Los objetos son "instancias" de una clase que se crean de manera similar a como se definen las variables:

`NombreClase nombreObjeto;`

Al crear un objeto tenemos disponibles las funciones miembro de la clase a la que pertenece el objeto.

###Funciones miembro de una clase

En general, en cada clase se definen los prototipos de funciones para construir los objetos, buscar, manipular y guardar los datos de la clase. 

`tipoDevolver nombreFunción(tipo de los parámetros);`

Luego, en el código del proyecto se escribe  la función, comenzando con un encabezado que incluye el nombre de la clase a la cuál pertenece la función:

`TipoDevolver NombreClase::NombreFunción(parámetros)`

Para que los objetos que sean instancia de una clase puedan tener acceso a las variables privadas de la clase se declaran funciones miembro que sean públicas y que den acceso a estas clases (ver abajo "setters" y "getters"). Es preferible utilizar variables privadas y accederlas mediante los "setters" y "getters" a declararlas públicas, ya que de esta manera el objeto que está asociado a estas variables tiene el control de los cambios que se hacen.

En la experiencia de laboratorio de funciones vimos como utilizar las funciones  `length` y `push_back` que son funciones miembro de la clase `string`. Vimos que para invocar la función escribimos el nombre del objeto, seguido de un punto y luego el nombre de la función:

`nombreObjeto.nombreFunción(argumentos);`

Esta es la manera usual de invocar funciones miembro de una clase. 


####Constructores

Las primeras funciones miembro de una clase que debemos entender son las *funciones constructores*. Estas funciones se invocan automáticamente cuando se crea un objeto con esa clase. Es una manera de inicializar el objeto con algunos valores por defecto para sus atributos. El saber cuáles son los constructores nos ayuda a entender cómo crearemos los objetos de esa clase. 

En C++, las funciones constructores tienen el mismo nombre que la clase. No se declara el tipo que devuelven porque estas funciones no devuelven ningún valor. Su declaración (incluida en la declaración de la clase) es algo así:

`nombreFunción(tipo de los parámetros);`

El encabezado de la función será algo así:

`NombreClase::NombreFunción(parámetros)`

La clase `Birds` que estaremos usando en la sesión de hoy tiene dos constructores:

`Bird (QWidget *parent=0)`

`Bird (int, EyeBrowType, QString, QString, QWidget *parent=0)`

Puedes ver las declaraciones de los prototipos de estas funciones en la declaración de la clase `Bird` en el archivo `bird.h` del proyecto. La documentación se encuentra [aqui.](http://ada.uprrp.edu/~ranazario/bird-html/class_bird.html)  El primer constructor, `Bird (QWidget *parent=0)`, es una función que se puede invocar con uno o ningún argumento. Si al invocarla no se usa argumento, el parámetro de la función toma el valor 0. 

El constructor de una clase que se puede invocar sin usar argumentos es el *constructor* "*default*" de la clase; esto es, el constructor que se invoca cuando creamos un objeto usando una oración como:

`Bird pitirre;`

Puedes ver las funciones y los parámetros que usan en el archivo `bird.cpp`. Nota que el primer constructor, `Bird (QWidget *parent=0)`, asignará valores aleartorios ("random") a cada uno de los atributos del objeto. Más adelante hay una breve explicación de la función `randInt`.

Dale un vistazo a la documentación del segundo constructor, `Bird (int, EyeBrowType, QString, QString, QWidget *parent=0)`. Esta función requiere cuatro argumentos y tiene un quinto argumento que es opcional. Una manera para usar este constructor es creando un objeto como el siguiente:

`Bird guaraguao(200, Bird::UPSET, "blue", "red");`


####"Setters" ("mutators")

Las clases proveen funciones para modificar los valores de los atributos de un objeto que se ha creado. Estas funciones se llaman "*setters*" o "*mutators*". Usualmente se declara un "setter" por cada atributo que tiene la clase. La clase `Bird` tiene los siguientes "setters":


* `void setEyeColor (QString)` 
* `void setFaceColor (QString)` 
* `void setEyebrow (EyeBrowType)` 
* `void setSize (int)` 

Puedes ver las declaraciones en la declaración de la clase `Bird` en  `bird.h` y las funciones en `bird.cpp`. El código en el siguiente ejemplo crea el objeto `bobo` de la clase `Bird` y luego cambia su tamaño a 333.

```
Bird bobo;
bobo.setSize(333);
```


####"Getters" ("accessors")

Las clases también proveen funciones para coger ("get") el valor del atributo de un objeto. Estas funciones se llaman "*getters*" o "*accessors*". Usualmente se declara un "getter" por cada atributo que tiene la clase. La clase `Bird` tiene los siguientes "getters":

* `QString   getEyeColor ()` 
* `QString  getFaceColor ()` 
* `int getSize ()` 
* `EyeBrowType getEyebrow ()` 

Puedes ver las declaraciones en la declaración de la clase `Bird` en  `bird.h` y las funciones en `bird.cpp`. El código en el siguiente ejemplo crea el objeto `piolin` de la clase `Bird` e imprime su tamaño.

```
Bird piolin;
cout << piolin.getSize();
```

####Otras funciones que utilizaremos

**MainWindow:** El archivo `mainwindow.h` contiene la declaración de una clase llamada `MainWindow`. Los objetos que sean instancias de esta clase podrán utilizar las funciones 

`void MainWindow::addBird(int x, int y, Bird &b)`  

`void MainWindow::addBird(Bird &b)` 

que añadirán a la pantalla un dibujo del objeto de la clase `Bird` que es recibido como argumento. El código en el siguiente ejemplo crea un objeto `w` de la clase `MainWindow`, crea un objeto `zumbador` de la clase `Bird` y lo añade a la posición (200,200) de la pantalla `w` usando la primera función.

```
MainWindow w;
Bird zumbador;
w.addBird(200,200,zumbador);
```



![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/FH7xwDQ.png)

**!Importante!** No es suficiente crear los objetos `Bird` para que éstos aparezcan en la pantalla. Es necesario usar una de las  funciones `addBird`  para que el dibujo aparezca.


**randInt:** La clase `Bird` incluye la función

`int Bird::randInt(int min, int max)`

para generar números enteros aleatorios ("random") en el rango [min, max]. La función `randInt` depende de otra función para generar números aleatorios que requiere un primer elemento o *semilla* para ser evaluada. En este proyecto, ese primer elemento se genera con la invocación `srand(time(NULL)) ;`.


###Ejercicio

En este ejercicio practicaremos como crear y manipular objetos utilizando la clase `Birds`.

####Instrucciones

1.	Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/Lab04-Objects.git` para descargar la carpeta `Lab04-Objects` a tu computadora.

2.	Marca doble "click" en el archivo `Cypher.pro` para cargar este proyecto a Qt. 

3. En el archivo `main.cpp` (en Sources) la función `main` hace lo siguiente:

  a. Crea un objeto aplicación de Qt, llamado `a`. Lo único que necesitas saber sobre este objeto es que gracias a él es que podemos crear una aplicación gráfica en Qt e interaccionar con ella.

  b. Crea un objeto instancia de la clase MainWindow llamado `w`. Este objeto corresponde a la ventana que veremos cuando corramos la aplicación.

  c. Inicializa la semilla del generador de números aleatorios de Qt. Esto hará que los pajaros nuevos tengan tamaños, colores y cejas aleatorias (a menos que los forcemos a tener valores específicos).

  d. Invocamos el método `show()` al objeto `w`. Esto logra que se muestre la ventana.

  e. En lugar de la instrucción `return 0;` como estamos acostumbrados en los programas que no tienen interface gráfica, damos la instrucción `return a.exec();` para que el objeto `a` se haga cargo de la aplicación a partir de ese momento.

4. Compila y corre la aplicación. Debes observar que se crea una ventana blanca.

5. Ahora crearemos un objeto de clase `Bird` llamado `abelardo`, usando el constructor default y lo añadimos a la ventana usando el método `addBird(int x, int y, Bird b)`. Copia el código que añadiste en la sección correspondiente de la [página de Entregas del Lab 4](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6654).

6. Corre varias veces el programa y maravíllate al ver a abelardo tener tamaños, colores y cejas distintas.

7. Utiliza los "setters" `setSize(int size)`, `setFaceColor(Qstring color)`, `setEyeColor(Qstring color)`,  and `setEyebrow(EyeBrowType)` para que abelardo luzca así (su size es 200).

  ![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/inDQ8tl.png)

  Copia el código que utilizaste para que abelardo luzca como en la imagen de arriba en la sección correspondiente de la [página de Entregas del Lab 4](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6654).

8. Crea otro objeto de tipo Bird llamado `piolin` que tenga cara azul, ojos verdes, y cejas UNI invocando el constructor `Bird(int size, EyeBrowType brow, QString faceColor, QString eyeColor, QWidget *parent = 0)`. Su tamaño debe ser la mitad que el de abelardo. Añadalo a la ventana usando `w.addBird(300, 100, piolin)`. 


  ![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/SMqYEoI.png)

  Copia el código que utilizaste para crear a piolin y añadirlo a la ventana donde está abelardo en la sección correspondiente de la [página de Entregas del Lab 4](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6654).



9. Crea otros dos objetos llamados `juana` y `alondra` que salgan dibujados en las coordenadas (100, 300) y (300,300) respectivamente. No asignes valores a las propiedades de juana, para que sean asignados aleatoriamente. `alondra` debe igual de grande que `juana`, tiene el mismo tipo de cejas, y el mismo color de ojos.  Su cara debe ser blanca.

  ![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/A5UVBHd.png)


  Copia el código que utilizaste para crear a alondra y a juana y añadirlo a la ventana donde están abelardo y piolin en la sección correspondiente de la [página de Entregas del Lab 4](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6654).

10. Corre varias veces el programa para asegurarte que `alondra` y `juana` siguen pareciéndose en tamaño, cejas y ojos. 

####Referencias:

https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-2




